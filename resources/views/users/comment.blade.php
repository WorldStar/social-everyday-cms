@extends('users.layout')
@section('content')

<div id="main_column" class="clear">
	<div class="clear mainbox-title-container">
		<h1 class="mainbox-title float-left">
			历史信息 
		</h1>
		
	</div>
	<div id="search-form" class="section-border">
		
			<table cellpadding="0" cellspacing="0" border="0" class="info_detail_view">
				<?php
                
				echo '<tr><td class="title">序号&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$users['id'].'</td>
					      <td class="title">用户名称&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$name.'</td></tr>';
				echo '<tr><td class="title">标题&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$users['commentnum'].'</td>
				          <td class="title">被攒&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$users['favonum'].'</td>
				          <td class="title">修改日期&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$users['modifydate'].'</td></tr>';				
				
			?>
			</table>
		
	</div>

	<div class="mainbox-body" >
		<div id="content_manage_users">
			<form action="/admin/index" method="GET">
			<div class="col_custom" >
				<div id="" class="grid-view">
					<div class="summary" style = "text-align:left;">
						<b>标题</b>
						
					</div>
					<table class="items">
						<thead>
							<tr>
								
								<th id="data_grid_view_c1">&nbsp;&nbsp;序号</th>
								<th id="data_grid_view_c1">缩略图预览</th>
								<th id="data_grid_view_c3">内&nbsp;&nbsp;容</th>
								<th id="data_grid_view_c3">日&nbsp;&nbsp;期</th>
								<!--<th id="data_grid_view_c7"><a class="sort-link" href="/index.php?r=user/admin&amp;User_sort=status">Status</a></th>-->
							</tr>
						</thead>	
						<tbody>
							<?php
								
								$i = 1;								
								foreach( $stage as $value )	
								{
									
									echo '<tr class="odd">';
									
									echo '<td>&nbsp;&nbsp;'.$i.'</td>';
									if($value['thumbnail'] == null){
										echo '<td width="10%"><a href="/uploads/images/'.$value['thumbnail'].'" target="_blank"><img src="/images/logo/icons2.png" width="120px" height="120px" style="margin:5px"/></a></td>';
									}else{
										echo '<td width="10%"><a href="/uploads/images/'.$value['thumbnail'].'" target="_blank"><img src="/uploads/images/'.$value['thumbnail'].'" width="120px" height="120px" style="margin:5px"/></a></td>';
									}
									echo '<td>'.$value['content'].'</a></td>';
									
									echo '<td>'.$value['modifydate'].'</td>';
									
									$i++;
								}

							?>

						</tbody>
					</table>
					
					
				</div>
			</div>
			<div class="col_custom" style = "float:right;">
				<div id="" class="grid-view">
					<div class="summary" style = "text-align:left;">
						<b size="20">内容</b>
						
					</div>
					<table class="items">
						<thead>
							<tr>
								
								<th id="data_grid_view_c1">&nbsp;&nbsp;序号</th>	
								<th id="data_grid_view_c1">标题用户名称</th>
								<th id="data_grid_view_c1">内&nbsp;&nbsp;容</th>
								<th id="data_grid_view_c3">日&nbsp;&nbsp;期</th>
								<!--<th id="data_grid_view_c7"><a class="sort-link" href="/index.php?r=user/admin&amp;User_sort=status">Status</a></th>-->
							</tr>
							<?php
								
								$i = 1;								
								foreach( $comment as $value )	
								{
									
									echo '<tr class="odd">';
									
									echo '<td>&nbsp;&nbsp;'.$i.'</td>';
									echo '<td>'.$value['username'].'</td>';
									echo '<td>'.$value['content'].'</a></td>';
									
									echo '<td>'.$value['modifydate'].'</td>';
									
									$i++;
								}

							?>
						</thead>	
						
					</table>
					
					
				</div>
			</div>	
				
			</form>
		</div>
	</div>
</div>

@stop
