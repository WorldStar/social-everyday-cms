@extends('users.layout')
@section('content')
<?php
	$input1 = "";
	$orderby1 = 0;
	if(!empty($input)){$input1=$input;}
	if(!empty($orderby)){$orderby1=$orderby;}
	

?>
<div id="main_column" class="clear">
	<div class="clear mainbox-title-container">
		<h1 class="mainbox-title float-left">
			历史记录 
		</h1>
		
	</div>
	<div id="search-form" class="section-border">
		<form name="user_search_form" action="/admin/historysearch" method="GET" class="">
			<table cellpadding="0" cellspacing="0" border="0" class="search-header">
				<tr>
					<td style="display:none">
						<input type="hidden" name="r" value="user/admin">
					</td>
					<td class="search-field nowrap">
						<!--<label for="elm_name">选择:</label>
						<div class="break">
							<select  name="select_option" id="select_option">
								<option value="1" selected = "selected" >用户名称</option>
								<option value="0" >标题</option>			
										
							</select>
						</div>-->
					</td>
					<td class="search-field nowrap">
						<label for="elm_mobile">搜索:</label>
						<div class="break">
							<input class="input-text" type="text" size="30" name="find" id="elm_mobile" value="<?php echo $input1;?>"/>
						</div>
					</td>
					<td class="search-field nowrap">
						<label for="elm_email">排序:</label>
						<div class="break">
							<select onchange="this.form.submit()" name="orderby_option" id="select_option">
								<option value="0" <?php if($orderby1 == 0){echo "selected";}?>>字序</option>
								<option value="1" <?php if($orderby1 == 1){echo "selected";}?>> 收藏</option>
								<option value="2"<?php if($orderby1 == 2){echo "selected";}?>>被攒</option>			
										
							</select>
						</div>
					</td>			
					<td class="buttons-container">
						<span  class="submit-button ">
							<input type="submit" name="mode" value="Search" style="display:none;"/>
						</span>
					</td>
				</tr>
			</table>
		</form>
	</div>

	<div class="mainbox-body" >
		<div id="content_manage_users">
			<form action="/admin/index" method="GET">
				<div id="data_grid_view" class="grid-view">
					<div class="summary">
						全部因素: {{ $users->count() }}
					</div>
					<table class="items">
						<thead>
							<tr>
								
								<th id="data_grid_view_c1">&nbsp;&nbsp;序号</th>
								<th id="data_grid_view_c1">图片</th>
								<th id="data_grid_view_c1">用户名称</th>
								<th id="data_grid_view_c1">修改日期</th>
								<th id="data_grid_view_c2">收藏星星 </th>
								<th id="data_grid_view_c2">星星 </th>
								<th id="data_grid_view_c2">被攒 </th>
								<th id="data_grid_view_c2">标题 </th>
								
								<!--<th id="data_grid_view_c7"><a class="sort-link" href="/index.php?r=user/admin&amp;User_sort=status">Status</a></th>-->
								
								
								
							</tr>
						</thead>	
						<tbody>
							<?php
								
								$i = 1;			
								foreach( $users as $value )	
								{
									echo '<tr class="odd">';
									
									echo '<td>&nbsp;&nbsp;'.$value['hid'].'</td>';
									if($value['thumbnail'] == null){
										echo '<td width="10%"><a href="/uploads/photos/'.$value['thumbnail'].'" target="_blank"><img src="/images/logo/e_icon.png" width="80px" height="80px" style="margin:5px"/></a></td>';
									}else{
										echo '<td width="10%"><a href="/uploads/photos/'.$value['thumbnail'].'" target="_blank"><img src="/uploads/photos/'.$value['thumbnail'].'" width="80px" height="80px" style="margin:5px"/></a></td>';
									}
									echo '<td><a class="view" href="/admin/huserprofile?pname='.$value['userno'].'&input='.$input1.'&orderby='.$orderby1.'">'.$value['username'].'</a></td>';
										
									
									echo '<td><a class="view" href="/admin/comment?hno='.$value['hid'].'&pname='.$value['userno'].'&input='.$input1.'&orderby='.$orderby1.'">'.$value['modifydate'].'</a></td>';
									echo '<td>'.$value['receivenum'].'</td>';									
									echo '<td>'.$value['pointnum'].'</td>';
									echo '<td>'.$value['favonum'].'</td>';
									echo '<td>'.$value['commentnum'].'</td>';
									
									
									//echo '<td>'.$value['friendnum'].'</td>';
									//echo '<td>'.$value['modifydate'].'</td>';
									//echo '<td width="8%"><a class="view" title="View user profile" href="/admin/userprofile?pname='.$value['id'].'"><img src="/images/customers.png" alt="View user profile" /></a>
									//&nbsp;&nbsp;&nbsp;<a class="view" title="View user contacts" href="/admin/contact?pname='.$value['id'].'"><img src="/images/usergroups.png" alt="View user contacts" /></a></td></tr>';
									//echo '<td width="8%"><input src="/index.php/userprofile'"><img src="/images/customers.png" alt="View user profile" /></td></tr>';
									
									$i++;
								}

							?>

						</tbody>
					</table>
					
					<div class="pager"><ul id="yw0" class="yiiPager">
						<?php echo $users->appends(Request::except('page'))->render(); ?>						
					</div>
				</div>
				
				
			</form>
		</div>
	</div>
</div>

@stop
