@extends('users.layout')
@section('content')
    <div id="main_column" class="clear">
        <div>
                

<div class="mainbox-body">
    <div class="cm-tabs-content">
	    <form class="cm-form-highlight">
            <h2 class="subheader">
                用户信息
            </h2>
			<?php
                echo '<table class="info_detail_view">';
				if($profile['thumbnail'] == null){
					echo '<tr><td class="title">缩略图预览&nbsp;&nbsp;&nbsp;:</td><td class="value"><img src="/images/logo/icons2.png" width="80px" height="80px" style="margin:5px"/></td></tr>';
				}else{
					echo '<tr><td class="title">缩略图预览&nbsp;&nbsp;&nbsp;:</td><td class="value"><img src="/uploads/photos/'.$profile['thumbnail'].'" width="80px" height="80px" style="margin:5px"/></td></tr>';
				}
				echo '<tr><td class="title">序号&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$profile['id'].'</td></tr>';
				echo '<tr><td class="title">全名&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$profile['fullname'].'</td></tr>';
				echo '<tr><td class="title">名称&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$profile['username'].'</td></tr>';
				echo '<tr><td class="title">邮箱&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$profile['email'].'</td></tr>';				
				echo '<tr><td class="title">手机&nbsp;&nbsp;&nbsp;:</td><td class="value">'. $profile['phone'] .'</td></tr>';
				echo '<tr><td class="title">生日&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$profile['birthday'].'</td></tr>';
				
				echo '<tr><td class="title">地址&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$profile['address'].'</td></tr>';
				echo '<tr><td class="title">supported &nbsp;&nbsp;&nbsp;:</td><td class="value">'.$profile['receivenum'].'</td></tr>';
				echo '<tr><td class="title">收藏 &nbsp;&nbsp;&nbsp;:</td><td class="value">'.$profile['sendnum'].'</td></tr>';
				echo '<tr><td class="title">星星 &nbsp;&nbsp;&nbsp;:</td><td class="value">'.$profile['pointnum'].'</td></tr>';
				echo '<td class="title">好友&nbsp;&nbsp;&nbsp;:</td><td class="value">'. $profile['friendnum'].'</td></tr>';
				echo '<tr><td class="title">修改日期&nbsp;&nbsp;&nbsp;:</td><td class="value">'.$profile['modifydate'].'</td></tr></table>';
			?>
			
                    
            <div class="buttons-container buttons-bg cm-toggle-button">
                <span class="cm-button-main cm-process-items">
                    <input type="button" onclick="location.href = '/admin/searchcontact?uid=<?php echo $uid;?>&find=<?php echo $input;?>&orderby_option=<?php echo $orderby;?>&select_option=<?php echo $option;?>'"  value="返回" />
                </span>
            </div>
            </form>
    </div>
</div>
        </div>
    </div>

@stop
