<?php
namespace App\Http\Controllers;


//use App\User;
use Illuminate\Http\Request;
use App\Models\Advertisement;
use App\Models\UserProfile;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\History;
use App\Models\User;
use App\Models\Comment;
use App\Models\Stage;
		
class HistoriesController extends Controller
{
	
	
	function index(Request $request)
	{	
		
		if(empty($_SESSION["login"]) || $_SESSION["login"] !== '1' )
		{
			return redirect()->intended('login');
		}	
		$item = $request->get('pageSize');
		
		if($item == null)
		{
			$item = 10;				
		}
		$table = new History();
		$users = $table->join('user','user.id', '=', 'history.userno')
					->select('user.*', 'history.id as hid', 'history.favonum','history.commentnum','history.modifydate','history.userno')->paginate($item);
		
									
		return view('users.histories')->with('users', $users)->with('item_v',$item);
	}
	function search(Request $request)
	{	
		if(empty($_SESSION["login"]) || $_SESSION["login"] !== '1' )
		{
			return redirect()->intended('login');
		}
		$item = $request->get('pageSize');
		
		if($item == null)
		{
			$item = 10;				
		}
		$table = new History();
		$input = $request->get('find');
		$orderby = $request->get('orderby_option');
		if(empty($input)){
			$users = $table->join('user','user.id', '=', 'history.userno')
				->select('user.*', 'history.id as hid', 'history.favonum','history.commentnum','history.modifydate','history.userno')->paginate($item);
			return view('users.histories')->with('users', $users)->with('item_v',$item);
		}else{
			if($orderby == '0')
				$users = $table->join('user','user.id', '=', 'history.userno')
				->select('user.*', 'history.id as hid', 'history.favonum','history.commentnum','history.modifydate','history.userno')
				->where('user.username', 'like', '%' . $input . '%')
				->orderBy('history.modifydate','DESC')
				->paginate($item);
			if($orderby == '1')
				$users = $table->join('user','user.id', '=', 'history.userno')
				->select('user.*', 'history.id as hid', 'history.favonum','history.commentnum','history.modifydate','history.userno')
				->where('user.username', 'like', '%' . $input . '%')
				->orderBy('user.receivenum','DESC')
				->paginate($item);
			if($orderby == '2')
				$users = $table->join('user','user.id', '=', 'history.userno')
				->select('user.*', 'history.id as hid', 'history.favonum','history.commentnum','history.modifydate','history.userno')
				->where('user.username', 'like', '%' . $input . '%')
				->orderBy('history.favonum','DESC')
				->paginate($item);
			return view('users.histories')->with('users', $users)->with('item_v',$item)->with('input',$input)->with('orderby',$orderby);
		}
		
	}
	
	function comment(Request $request)
	{
		if(empty($_SESSION["login"]) || $_SESSION["login"] !== '1' )
		{
			return redirect()->intended('login');
		}
		$userno = $request->get('pname');
		$input = $request->get('input');
		$orderby = $request->get('orderby');
		$hno = $request->get('hno');
		$userinfo = User::where('id', '=', $userno)->first();
		$username = $userinfo->username;
		$historyinfo = History::where('id', '=', $hno) -> first();
		$stage = Stage::where('hno', '=', $hno)->orderBy('modifydate','DESC')->get();
		$table = new Comment();
				$comment = $table->join('user','user.id', '=', 'comment.userno')
				->select('comment.*', 'user.username')
				->where('comment.hno', '=', $hno)
				->orderBy('modifydate','DESC')->get();
		//$comment = Comment::where('hno', '=', $hno)
		
		return view('users.comment')->with('users',$historyinfo)->with('name',$username)->with('stage',$stage)->with('comment',$comment)->with('input',$input)->with('orderby',$orderby);
	}

	function ad_register(Request $request)
	{	
		if(empty($_SESSION["login"]) || $_SESSION["login"] !== '1' )
		{
			return redirect()->intended('login');
		}	

		$adver = new Advertisement();
									
		return view('users.ad_register')->with('adver',$adver)->with('error', '')->with('mode', 'create');
	}
	
	function addItem(Request $request)
	{
		if(empty($_SESSION["login"]) || $_SESSION["login"] !== '1' )
		{
			return redirect()->intended('login');
		}	
		
		$data = $request->get('region_data');
		
		$adver = new Advertisement();
		
		$adver->title = $data['title'];
		$adver->thumbpath = $data['thumb_url'];
		$adver->start = $data['start'];
		$adver->end = $data['end'];
		$adver->sequence = $data['sequence'];
		
		$adver->save();
		
		$adver = new Advertisement(); 
		return view('users.ad_register')->with('adver',$adver)->with('error', 'SUCCESS')->with('mode', 'create');
	}
	
    function ad_edit(Request $request, $id)
	{	
		if(empty($_SESSION["login"]) || $_SESSION["login"] !== '1' )
		{
			return redirect()->intended('login');
		}	

		$page = $request->get('page');
		if( $page === null )
			$page = 1;
		
		$adver = Advertisement::find($id);
		if( $adver == null )
		{
			return redirect()->intended('admin/advertisement?page='.$page);
		}
									
		return view('users.ad_register')->with('adver',$adver)->with('error', '')->with('mode', 'edit');

	}		

    function updateItem(Request $request, $id)
    {	
		if(empty($_SESSION["login"]) || $_SESSION["login"] !== '1' )
		{
			return redirect()->intended('login');
		}	
		
		$adver = Advertisement::find($id);
		if( $adver == null )
		{
			return redirect()->intended('admin/advertisement');
		}
		
		$data = $request->get('region_data');
		
		$adver->title = $data['title'];
		$adver->thumbpath = $data['thumb_url'];
		$adver->start = $data['start'];
		
		$adver->end = $data['end'];
		$adver->sequence = $data['sequence'];
		
		
		$adver->save();
		
		return view('users.ad_register')->with('adver',$adver)->with('error', 'SUCCESS')->with('mode', 'edit');
	}		

    function ad_delete(Request $request, $id)
	{		
		if(empty($_SESSION["login"]) || $_SESSION["login"] !== '1' )
		{
			return redirect()->intended('login');
		}	
		
		$page = $request->get('page');
		if( $page === null )
			$page = 1;

		$my_delete = Advertisement::find($id);

		if( $my_delete != null )
			$my_delete->delete();

		
		return redirect()->intended('admin/advertisement?page='.$page);		
	}			
	
	function ad_totaldelete(Request $request)
	{		
		if(empty($_SESSION["login"]) || $_SESSION["login"] !== '1' )
		{
			return redirect()->intended('login');
		}	
		
		$deletions = $_POST['cat_ids'];
		Advertisement::destroy($deletions);
		
		return redirect()->intended('admin/advertisement');		
	}
	
}
