<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \DateTime;
use \DateInterval;
use App\Models\OfflineMessage;


class CommentStar extends Model { 
	protected $table = 'commentstar';
	//protected $table = 'advertisement';
	public static $error = 0;
	
	public static function addCommentNum($userno, $hno)
	{	
		$history = History::where('id', '=', $hno) ->first();
		$huserno = $history->userno;
		
		$stars = CommentStar::where('userno', '=', $userno)-> where('huserno', '=', $huserno) ->first();
		
		if(empty($stars)){
			// create user
			$stars = new CommentStar();
			$stars->userno = $userno;	
			$stars->huserno = $huserno;			
			$stars->commentnum = 1;
			$stars->save();
			
		}else{
			$num = $stars->commentnum + 1;
			if($num == 5){
				$stars->commentnum = 0;
				$stars->save();
				$user = User::where('id', '=', $huserno) ->first();
				$commentstarnum = $user->commentstarnum;
				
				SendPoint::addSendPoint($huserno, $userno, $commentstarnum);
				User::sumSendNum($huserno, $commentstarnum);	
				User::sumReceiveNum($userno, $commentstarnum);
			}else{
				$stars->commentnum = $num;
				$stars->save();
			}
		}
		//History::updateCommentNum($hno);
		
	}	
}