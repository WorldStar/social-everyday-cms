<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \DateTime;
use \DateInterval;
use App\Models\OfflineMessage;


class Verify extends Model { 
	protected $table = 'verifycode';
	public static $error = 0;
	
	public static function saveVerify($email, $vcode)
	{
		$verify = Verify::where('email', '=', $email)->first();
		if( empty($verify) )	// user does not exist
		{
			$verify = new Verify();
			$verify -> email = $email;
			$verify -> vcode = $vcode;
			$verify -> save();
		}else{
			$verify -> vcode = $vcode;
			$verify -> save();
		}		
	}
	
}