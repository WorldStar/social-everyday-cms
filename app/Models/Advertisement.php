<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \DateTime;
use \DateInterval;
use App\Models\OfflineMessage;


class Advertisement extends Model { 
	protected $table = 'advertisement';
	public static $error = 0;
	
	public function comments()
    {
        return $this->hasMany('App\Models\OfflineMessage', 'username', 'username');
    }
	public static function getAdvertisement($userno, $pagenum, $limited)
	{
		
		$Ads = Advertisement::orderBy('published','DESC')-> offset($pagenum)->limit($limited)->get();
		$dataAds = array();
		$i = 0;
		foreach($Ads as $advertisement){
			$dataAds[$i]['id'] = $advertisement->id;
			$dataAds[$i]['title'] = $advertisement->title;
			$dataAds[$i]['thumbpath'] = $advertisement->thumbpath;
			$dataAds[$i]['sequence'] = $advertisement->sequence;
			$dataAds[$i]['published'] = $advertisement->published;
			
			$i++;
		}
		return $dataAds;
	}
}