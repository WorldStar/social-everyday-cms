<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \DateTime;
use \DateInterval;
use App\Models\OfflineMessage;


class History extends Model { 
	protected $table = 'history';
	public static $error = 0;
	
	protected $fillable = array('fullname', 'username', 'password', 'email', 'phone', 'thumbnail', 'birthday', 'address', 'supportnum', 'pointnum', 'commentnum', 'frinenum', 'token', 'modifydate');
	public $timestamps = false;
	
	public function comments()
    {
        return $this->hasMany('App\Models\OfflineMessage', 'username', 'username');
    }
	
	public static function addHistory($userno,$content)
	{
		
		// create user
		$history = new History();
		$history->userno = $userno;			
		$history->content = $content;
		
		$history->save();
		return 1;
	}
	public static function getRecentHistory($userno, $pagenum, $limited)
	{
		
		$histories = History::orderBy('modifydate','DESC')-> offset($pagenum)->limit($limited)->get();
		$datahistories = array();
		$i = 0;
		foreach($histories as $history){
			$datahistories[$i]['id'] = $history->id;
			$datahistories[$i]['huserno'] = $history->userno;
			$datahistories[$i]['content'] = $history->content;
			$datahistories[$i]['favonum'] = $history->favonum;
			$datahistories[$i]['commentnum'] = $history->commentnum;
			$datahistories[$i]['modifydate'] = $history->modifydate;
			
			$stage= Stage::getStage($history->id);
			if(empty($stage)){
				$datahistories[$i]['thumbnail'] = "";			
				//$datahistories[$i]['content'] = "";
			}else{
				$datahistories[$i]['thumbnail'] = $stage->thumbnail;	
				
				//$datahistories[$i]['content'] = $stage->content;
			}
			$point = SendPoint::where('huserno', '=', $history->userno)->where('hno', '=', $history->id)->get();
			$sum = 0;
			foreach($point as $point1){
				$sum = $sum + $point1->amount;
			}
			$datahistories[$i]['receivenum'] = $sum;
			$user = User::where('id', '=', $history->userno)->first();
			$datahistories[$i]['fullname'] = $user->fullname;
			$datahistories[$i]['username'] = $user->username;
			$datahistories[$i]['email'] = $user->email;
			$datahistories[$i]['userphoto'] = $user->thumbnail;
			$datahistories[$i]['address'] = $user->address;
			$datahistories[$i]['sendnum'] = $user->sendnum;
			$datahistories[$i]['pointnum'] = $user->pointnum;
			$friend = Contacts::checkContact($userno, $history->userno);			
			$datahistories[$i]['checkfriend'] = $friend;
			$user = User::where('id', '=', $history->userno)->first();
			$favo = Favorite::getOneFavorite($userno, $history->id);
			$datahistories[$i]['favorited'] = $favo;
			$i++;
		}
		return $datahistories;
	}
	public static function getPreRecentHistory($pagenum, $limited)
	{
		
		$histories = History::orderBy('modifydate','DESC')-> offset($pagenum)->limit($limited)->get();
		$datahistories = array();
		$i = 0;
		foreach($histories as $history){
			$datahistories[$i]['id'] = $history->id;
			$datahistories[$i]['huserno'] = $history->userno;
			$datahistories[$i]['content'] = $history->content;
			$datahistories[$i]['favonum'] = $history->favonum;
			$datahistories[$i]['commentnum'] = $history->commentnum;
			$datahistories[$i]['modifydate'] = $history->modifydate;
			
			$stage= Stage::getStage($history->id);
			if(empty($stage)){
				$datahistories[$i]['thumbnail'] = "";			
				//$datahistories[$i]['content'] = "";
			}else{
				$datahistories[$i]['thumbnail'] = $stage->thumbnail;	
				
				//$datahistories[$i]['content'] = $stage->content;
			}
			
			$point = SendPoint::where('huserno', '=', $history->userno)->where('hno', '=', $history->id)->get();
			$sum = 0;
			foreach($point as $point1){
				$sum = $sum + $point1->amount;
			}
			$datahistories[$i]['receivenum'] = $sum;
			$user = User::where('id', '=', $history->userno)->first();
			$datahistories[$i]['fullname'] = $user->fullname;
			$datahistories[$i]['username'] = $user->username;
			$datahistories[$i]['email'] = $user->email;
			$datahistories[$i]['userphoto'] = $user->thumbnail;
			$datahistories[$i]['address'] = $user->address;
			$datahistories[$i]['sendnum'] = $user->sendnum;
			$datahistories[$i]['pointnum'] = $user->pointnum;			
			$datahistories[$i]['checkfriend'] = 0;
			$datahistories[$i]['favorited'] = 0;
			$i++;
		}
		return $datahistories;
	}
	public static function getSearchHistory($userno, $searchkey, $pagenum, $limited)
	{    
		// get History       where('name', 'LIKE', '%'.$name.'%')
		if(!empty($searchkey))
			$histories = History::where('content','LIKE','%'.$searchkey.'%')->orderBy('modifydate','DESC')-> offset($pagenum)->limit($limited)->get();
		else
			$histories = History::orderBy('modifydate','DESC')-> offset($pagenum)->limit($limited)->get();//all();
		
		$datahistories = array();
		$i = 0;
		foreach($histories as $history){
			$datahistories[$i]['id'] = $history->id;
			$datahistories[$i]['huserno'] = $history->userno;
			$datahistories[$i]['content'] = $history->content;
			$datahistories[$i]['favonum'] = $history->favonum;
			$datahistories[$i]['commentnum'] = $history->commentnum;
			$datahistories[$i]['modifydate'] = $history->modifydate;
			
			$stage= Stage::getStage($history->id);
			if(empty($stage)){
				$datahistories[$i]['thumbnail'] = "";			
				//$datahistories[$i]['content'] = "";
			}else{
				$datahistories[$i]['thumbnail'] = $stage->thumbnail;	
				
				//$datahistories[$i]['content'] = $stage->content;
			}
			$point = SendPoint::where('huserno', '=', $history->userno)->where('hno', '=', $history->id)->get();
			$sum = 0;
			foreach($point as $point1){
				$sum = $sum + $point1->amount;
			}
			$datahistories[$i]['receivenum'] = $sum;
			$user = User::where('id', '=', $history->userno)->first();
			$datahistories[$i]['fullname'] = $user->fullname;
			$datahistories[$i]['username'] = $user->username;
			$datahistories[$i]['email'] = $user->email;
			$datahistories[$i]['userphoto'] = $user->thumbnail;
			$datahistories[$i]['address'] = $user->address;
			$datahistories[$i]['sendnum'] = $user->sendnum;
			$datahistories[$i]['pointnum'] = $user->pointnum;
			$friend = Contacts::checkContact($userno, $history->userno);			
			$datahistories[$i]['checkfriend'] = $friend;
			$user = User::where('id', '=', $history->userno)->first();
			$favo = Favorite::getOneFavorite($userno, $history->id);
			$datahistories[$i]['favorited'] = $favo;
			$i++;
		}
		return $datahistories;
	}
	public static function getHistorybyUser($userno, $huserno, $pagenum, $limited)
	{
		
		// get History
		$histories = History::where('userno', '=', $huserno) -> orderBy('modifydate','DESC')-> offset($pagenum)->limit($limited)->get();
		$datahistories = array();
		$i = 0;
		foreach($histories as $history){
			$datahistories[$i]['id'] = $history->id;
			$datahistories[$i]['huserno'] = $history->userno;
			$datahistories[$i]['content'] = $history->content;
			$datahistories[$i]['favonum'] = $history->favonum;
			$datahistories[$i]['commentnum'] = $history->commentnum;
			$datahistories[$i]['modifydate'] = $history->modifydate;
			$stage= Stage::getStage($history->id);
			if(empty($stage) )	// user does not exist
			{
				$datahistories[$i]['thumbnail'] = "";
				//$datahistories[$i]['content'] = "";
			}else{
				$datahistories[$i]['thumbnail'] = $stage->thumbnail;
				//$datahistories[$i]['content'] = $stage->content;
				
			}
			$point = SendPoint::where('huserno', '=', $history->userno)->where('hno', '=', $history->id)->get();
			$sum = 0;
			foreach($point as $point1){
				$sum = $sum + $point1->amount;
			}
			$datahistories[$i]['receivenum'] = $sum;
			$user = User::where('id', '=', $history->userno)->first();
			$datahistories[$i]['fullname'] = $user->fullname;
			$datahistories[$i]['username'] = $user->username;
			$datahistories[$i]['email'] = $user->email;
			$datahistories[$i]['userphoto'] = $user->thumbnail;
			$datahistories[$i]['address'] = $user->address;
			$datahistories[$i]['sendnum'] = $user->sendnum;
			$datahistories[$i]['pointnum'] = $user->pointnum;
			$friend = Contacts::checkContact($userno, $history->userno);
			$datahistories[$i]['checkfriend'] = $friend;
			$favo = Favorite::getOneFavorite($userno, $history->id);
			$datahistories[$i]['favorited'] = $favo;
			$i++;
		}
		return $datahistories;
	}
	public static function getOwnHistory($userno, $pagenum, $limited)
	{
		
		// get History
		$datahistories = array();
		if($pagenum == 0){
			$datahistories[0]['id'] = 0;
			$datahistories[0]['favonum'] = 0;
			$datahistories[0]['commentnum'] = 0;
			$datahistories[0]['modifydate'] = 0;
			$datahistories[0]['content'] = "";
			$stages= TempStage::getStages($userno);
			
			$thumbarray = array();
			if(empty($stages)){
				$datahistories[0]['thumbnail'] = $thumbarray;			
				//$datahistories[$i]['content'] = "";
			}else{
				$k = 0;
				foreach($stages as $stage){
					$thumbarray[$k] = $stage->thumbnail;
					if($k == 3){
						break;
					}
					$k++;
				}
				$datahistories[0]['thumbnail'] = $thumbarray;	
				
				
				//$datahistories[$i]['content'] = $stage->content;
			}
			$i = 1;
		}else{
			$i = 0;
		}
		$histories = History::where('userno', '=', $userno) -> orderBy('modifydate','DESC')-> offset($pagenum)->limit($limited)->get();
		
		foreach($histories as $history){
			$datahistories[$i]['id'] = $history->id;
			$datahistories[$i]['content'] = $history->content;
			$datahistories[$i]['favonum'] = $history->favonum;
			$datahistories[$i]['commentnum'] = $history->commentnum;
			$datahistories[$i]['modifydate'] = $history->modifydate;
			
			$point = SendPoint::where('huserno', '=', $userno)->where('hno', '=', $history->id)->get();
			$sum = 0;
			foreach($point as $point1){
				$sum = $sum + $point1->amount;
			}
			$datahistories[$i]['receivenum'] = $sum;
			$stages= Stage::getStages($userno, $history->id);
			
			$thumbarray = array();
			if(empty($stages)){
				$datahistories[$i]['thumbnail'] = $thumbarray;			
				//$datahistories[$i]['content'] = "";
			}else{
				$k = 0;
				foreach($stages as $stage){
					$thumbarray[$k] = $stage->thumbnail;
					if($k == 3){
						break;
					}
					$k++;
				}
				$datahistories[$i]['thumbnail'] = $thumbarray;	
				
				//$datahistories[$i]['content'] = $stage->content;
			}
			$i++;
		}
		return $datahistories;
	}
	public static function getHighUsers($userno, $pagenum, $limited)
	{
		// get History 
		$users = User::orderBy('sortscore','DESC')-> offset($pagenum)->limit($limited)->get();
		$datahistories = array();
		$i = 0;
		foreach($users as $user){
			$datahistories[$i]['id'] = $user->id;
			$datahistories[$i]['fullname'] = $user->fullname;
			$datahistories[$i]['username'] = $user->username;
			$datahistories[$i]['email'] = $user->email;
			$datahistories[$i]['userphoto'] = $user->thumbnail;
			$datahistories[$i]['address'] = $user->address;
			$datahistories[$i]['receivenum'] = $user->receivenum;
			$datahistories[$i]['sendnum'] = $user->sendnum;
			$datahistories[$i]['pointnum'] = $user->pointnum;
			$friend = Contacts::checkContact($userno, $user->id);
			$datahistories[$i]['checkfriend'] = $friend;			
			$history = History::where('userno', '=', $user->id) ->first();
			if(empty($history) )	// user does not exist
			{
				$datahistories[$i]['thumbnail'] = "";
				$datahistories[$i]['content'] = "";		
				$datahistories[$i]['content'] = "";				
			}else{
				$datahistories[$i]['modifydate'] = $history->modifydate;
				$datahistories[$i]['content'] = $history->content;
				$stage= Stage::getStage($history->id);
				if(empty($stage) )	// user does not exist
				{
					$datahistories[$i]['thumbnail'] = "";
					//$datahistories[$i]['content'] = "";
				}else{
					$datahistories[$i]['thumbnail'] = $stage->thumbnail;
					//$datahistories[$i]['content'] = $stage->content;
				}
			}	
			$i++;
		}
		return $datahistories;
	}
	public static function getReceivenumUsers($userno, $pagenum, $limited)
	{
		$users = User::orderBy('receivenum','DESC')-> offset($pagenum)->limit($limited)->get();
		$datahistories = array();
		$i = 0;
		foreach($users as $user){
			$datahistories[$i]['id'] = $user->id;
			$datahistories[$i]['fullname'] = $user->fullname;
			$datahistories[$i]['username'] = $user->username;
			$datahistories[$i]['email'] = $user->email;
			$datahistories[$i]['userphoto'] = $user->thumbnail;
			$datahistories[$i]['address'] = $user->address;
			$datahistories[$i]['receivenum'] = $user->receivenum;
			$datahistories[$i]['sendnum'] = $user->sendnum;
			$datahistories[$i]['pointnum'] = $user->pointnum;
			$friend = Contacts::checkContact($userno, $user->id);
			$datahistories[$i]['checkfriend'] = $friend;			
			$history = History::where('userno', '=', $user->id) ->first();
			if(empty($history) )	// user does not exist
			{
				$datahistories[$i]['thumbnail'] = "";
				$datahistories[$i]['content'] = "";		
				$datahistories[$i]['content'] = "";				
			}else{
				$datahistories[$i]['modifydate'] = $history->modifydate;
				$datahistories[$i]['content'] = $history->content;
				$stage= Stage::getStage($history->id);
				if(empty($stage) )	// user does not exist
				{
					$datahistories[$i]['thumbnail'] = "";
					//$datahistories[$i]['content'] = "";
				}else{
					$datahistories[$i]['thumbnail'] = $stage->thumbnail;
					//$datahistories[$i]['content'] = $stage->content;
				}
			}	
			$i++;
		}
		return $datahistories;
	}
	public static function getSendnumUsers($userno, $pagenum, $limited)
	{
		$users = User::orderBy('sendnum','DESC')-> offset($pagenum)->limit($limited)->get();
		$datahistories = array();
		$i = 0;
		foreach($users as $user){
			$datahistories[$i]['id'] = $user->id;
			$datahistories[$i]['fullname'] = $user->fullname;
			$datahistories[$i]['username'] = $user->username;
			$datahistories[$i]['email'] = $user->email;
			$datahistories[$i]['userphoto'] = $user->thumbnail;
			$datahistories[$i]['address'] = $user->address;
			$datahistories[$i]['receivenum'] = $user->receivenum;
			$datahistories[$i]['sendnum'] = $user->sendnum;
			$datahistories[$i]['pointnum'] = $user->pointnum;
			$friend = Contacts::checkContact($userno, $user->id);
			$datahistories[$i]['checkfriend'] = $friend;			
			$history = History::where('userno', '=', $user->id) ->first();
			if(empty($history) )	// user does not exist
			{
				$datahistories[$i]['thumbnail'] = "";
				$datahistories[$i]['content'] = "";		
				$datahistories[$i]['content'] = "";				
			}else{
				$datahistories[$i]['modifydate'] = $history->modifydate;
				$datahistories[$i]['content'] = $history->content;
				$stage= Stage::getStage($history->id);
				if(empty($stage) )	// user does not exist
				{
					$datahistories[$i]['thumbnail'] = "";
					//$datahistories[$i]['content'] = "";
				}else{
					$datahistories[$i]['thumbnail'] = $stage->thumbnail;
					//$datahistories[$i]['content'] = $stage->content;
				}
			}	
			$i++;
		}
		return $datahistories;
	}
	
	public static function updateFavoNum($hno)
	{
		$history = History::where('id', '=', $hno) ->first();
		
		if( empty($history) )	// user does not exist
		{
			return -1;
		}else{
			// update user	
			$history->favonum = $history->favonum + 1;
		}				
			
		$history->save();
		return 1;
	}
		
	public static function getHistories($hno, $userno)
	{
		$history = History::where('id', '=', $hno) -> where('userno', '=', $userno) ->first();
		
		if( empty($history) )
			return -1;
		$history -> favonum = $history -> favonum - 1;
		$history->delete();
		
		return 1;
	}
		
	public static function deleteFavoNum($hno)
	{
		$history = History::where('id', '=', $hno) ->first();
		
		if( empty($history) )
			return -1;
		$history -> favonum = $history -> favonum - 1;
		$history->delete();
		
		return 1;
	}
	public static function updateCommentNum($hno)
	{
		$history = History::where('id', '=', $hno) ->first();
		
		if( empty($history) )	// user does not exist
		{
			return -1;
		}else{
			// update user	
			$history->commentnum = $history->commentnum + 1;
		}				
			
		$history->save();
		return 1;
	}
	
		
	public static function deleteCommentNum($hno)
	{
		$history = History::where('id', '=', $hno) ->first();
		
		if( empty($history) )
			return -1;
		$history -> commentnum = $history -> commentnum - 1;
		$history->delete();
		
		return 1;
	}
	public static function deleteHistory($hno, $userno)
	{
		$history = History::where('id', '=', $hno) -> where('userno', '=', $userno) ->first();
		
		if( empty($history) )
			return -1;
		$history->delete();
		
		return 1;
	}
}