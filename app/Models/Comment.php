<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \DateTime;
use \DateInterval;
use App\Models\OfflineMessage;


class Comment extends Model { 
	protected $table = 'comment';
	//protected $table = 'advertisement';
	public static $error = 0;
	
	protected $fillable = array('fullname', 'username', 'password', 'email', 'phone', 'thumbnail', 'birthday', 'address', 'supportnum', 'pointnum', 'commentnum', 'frinenum', 'token', 'modifydate');
	public $timestamps = false;
	
	/* public function comments()
    {
        return $this->hasMany('App\Models\OfflineMessage', 'username', 'username');
    } */
	
	public static function addComment($hno, $userno, $content)
	{
		
		// create user
		$stage = new Comment();
		$stage->userno = $userno;	
		$stage->hno = $hno;	
		$stage->content = $content;			
			
		$id = $stage->save();
		History::updateCommentNum($hno);
		return $id;
	}	
	public static function deleteComment($cno, $hno)
	{
		$stage = Comment::where('id', '=', $cno)-> where('userno', '=', $userno) ->first();
		
		if( empty($stage) )
			return -1;
		$stage->delete();
		History::deleteCommentNum($hno);
		
		return 1;
	}
	public static function getComments($hno, $pagenum, $limit)
	{
		//limit(30)->offset(30)->get();
		//old take(30)->skip(30)->get();
		//$stage = TempStage::where('hno', '=', $hno) -> where('userno', '=', $userno) ->first();
		
		$stages = Comment::where('hno', '=', $hno) -> offset($pagenum)->limit($limit)->get();
		$comments = array();
		$i = 0;
		foreach($stages as $stage){
			$comments[$i]['id'] = $stage->id;
			$comments[$i]['content'] = $stage->content;
			$comments[$i]['modifydate'] = $stage->modifydate;
			$user = User::where('id', '=', $stage->userno)->first();
			$comments[$i]['fullname'] = $user->fullname;
			$comments[$i]['username'] = $user->username;
			$comments[$i]['email'] = $user->email;
			$comments[$i]['pointnum'] = $user->pointnum;
			$comments[$i]['sendnum'] = $user->sendnum;
			$comments[$i]['userphoto'] = $user->thumbnail;
			$i++;
		}
		
		
		return $comments;
	}
}